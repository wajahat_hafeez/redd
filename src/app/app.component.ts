import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { OfficesPage } from '../pages/offices/offices';
import { Network } from '@ionic-native/network';
import { NetworkProvider } from '../providers/network/network';
import { Events } from 'ionic-angular';
import { SelectLanguagePage } from '../pages/select-language/select-language';
import { YourRightsPage } from '../pages/your-rights/your-rights';
import { FaqPage } from '../pages/faq/faq';
import { TestPage } from '../pages/test/test';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SelectLanguagePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public networkProvider: NetworkProvider, public network:Network, public events:Events) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Your Rights', component: YourRightsPage },
      { title: 'Grievance', component: ContactUsPage },
      { title: 'F.A.Q', component: FaqPage },
      { title: 'Contact Us', component: OfficesPage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      //this.statusBar.overlaysWebView(true);
      this.statusBar.backgroundColorByHexString('#00b300');
      this.splashScreen.hide();

      this.networkProvider.initializeNetworkEvents();

	       		// Offline event
			    this.events.subscribe('network:offline', () => {
			        alert('The network appears to be offline, Please check your internet connection.');    
			    });

			    // Online event
			    this.events.subscribe('network:online', () => {
			        //alert('network:online ==> '+this.network.type);        
			    });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
