import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, LoadingController, AlertController } from 'ionic-angular';
 import { CallNumber } from '@ionic-native/call-number';
 import { SMS } from '@ionic-native/sms';
 import { EmailComposer } from '@ionic-native/email-composer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { SelectLanguagePage } from '../pages/select-language/select-language';


import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { ModelProvider } from '../providers/model/model';
import {HttpClientModule} from '@angular/common/http';
import { OfficesPage } from '../pages/offices/offices';
import { HelperPage } from '../pages/helper/helper';
import { NetworkProvider } from '../providers/network/network';
import { Network } from '@ionic-native/network';
import { YourRightsPage } from '../pages/your-rights/your-rights';
import { FaqPage } from '../pages/faq/faq';
import { DescriptionPage } from '../pages/description/description';
import { TestPage } from '../pages/test/test';

import { FormsModule } from '@angular/forms';

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ContactUsPage,
    OfficesPage,
    HelperPage,
    SelectLanguagePage,
    YourRightsPage,
    FaqPage,
    DescriptionPage,
    TestPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ContactUsPage,
    OfficesPage,
    HelperPage,
    SelectLanguagePage,
    YourRightsPage,
    FaqPage,
    DescriptionPage,
    TestPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    LoadingController,
    AlertController,
    CallNumber,
    SMS,
    EmailComposer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ModelProvider,
    NetworkProvider,
    Network,
    File,
    FileOpener
  ]
})
export class AppModule {}
