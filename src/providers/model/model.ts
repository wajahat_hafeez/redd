import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

/*
  Generated class for the ModelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ModelProvider {

  data:any;
  baseUrl:string;
  constructor(public http: HttpClient) {
    console.log('Hello ModelProvider Provider');
  }

  loadGet(url) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }


    // don't have the data yet
  return new Promise((resolve,reject) => {
    
    this.http.get(url)
    .timeout(1000*10)
    .toPromise().then(data => {
        
        this.data = data;
        resolve(this.data);
      })
      .catch(error =>
        {
          console.log(error);
        reject(error);
      });
  });

  }


  loadPost(url, param) {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }

    let headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }); 

    var config = {
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
      }
  }
    
    // don't have the data yet
  return new Promise((resolve,reject) => {  
    
    this.http.post(url,param,{headers:headers})
    .timeout(1000*10)
    .toPromise().then(data => {
        
      console.log("Hello");
        this.data = data;
        resolve(this.data);
      })
      .catch(error =>
        {
          console.log(error);
          reject(error);
      });
  });

  }

}
