import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-description',
  templateUrl: 'description.html',
})
export class DescriptionPage {

  data;

  gettitle:string;
  getdesc:string;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.data = navParams.get('data');

    console.log(this.data);

    var title = this.data.title;
    var desc = this.data.desc;

    this.gettitle = title.replace(/\[(.*)\]/g, '<sup>$1</sup>');
    this.getdesc = desc.replace(/\[(.*)\]/g, '<sup>$1</sup>');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionPage');
  }

}
