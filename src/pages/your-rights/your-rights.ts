import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DescriptionPage } from '../description/description';

/**
 * Generated class for the YourRightsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-your-rights',
  templateUrl: 'your-rights.html',
})
export class YourRightsPage {

  changeColor: string;

  rights:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YourRightsPage');


    this.rights= [
      {title: 'Right to Information', imgsrc: 'assets/imgs/green(3).png',desc:'Right to access information is recognized and protected in the context of the implementation of the REDD<sup>+</sup> strategy.\nYou have a right to access information generally about REDD<sup>+</sup> and any REDD<sup>+</sup> activities, in as this is a qualified right under the National Constitution of Pakistan.\nThe National REDD<sup>+</sup> office and REDD<sup>+</sup> Provincial Management Units are committed to providing any request of information and proactively disseminating relevant information about the implementation of the REDD<sup>+</sup> Strategy.' },
      {title: 'Right to Participation', imgsrc: 'assets/imgs/green(2).png',desc:'You have a right to participate in the design and implementation of the REDD<sup>+</sup> strategy if you are an “interested party” (those having a bonafide interest in the sustainable development of the forest and natural resources).\nThe government must also facilitate the participation of village communities and interested parties in the sustainable development of forests and wastelands and ensure the participation and assistance of communities in the regeneration of cut-over areas, particularly owners, right holders, users, and women.' },
      {title: 'Right to Justice', imgsrc: 'assets/imgs/tree.svg',desc:'Right to access justice is recognized and protected in the context of the implementation of the REDD<sup>+</sup> strategy.\nThe Constitution of Pakistan ensures that to enjoy the protection of law and to be treated in accordance with law is the inalienable right of every citizen.\nFor the determination of his civil rights and obligations or in any criminal charge against him a person shall be entitled to a fair trial and due process.'},
      {title: 'Gender Equality', imgsrc: 'assets/imgs/green(5).png',desc:'Gender equality is promoted and protected in the context of the implementation of the REDD<sup>+</sup> strategy.\nThe Constitution recognizes Gender Equality and the Right of women citizens to participate in all economic activities.' },
      {title: 'Land Rights', imgsrc: 'assets/imgs/green(4).png',desc:'Rights over forest land are recognized and protected in the context of the implementation of the REDD<sup>+</sup> strategy.\nThe Constitution of Pakistan guarantees that every citizen shall have the right to acquire, hold and dispose of the property in any part of Pakistan, subject to the Constitution and any reasonable restrictions imposed by law in the public interest.\nForestry laws in Pakistan ensure tenure and rights of local communities over state owned forests including right of way, water, firewood etc.'}
  ];

  }

  goTo(id)
  {
    //this.changeColor = 'green';
    this.navCtrl.push(DescriptionPage, {
      data: this.rights[id]
    });
  }

}
