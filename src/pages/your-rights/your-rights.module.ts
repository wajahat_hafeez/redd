import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YourRightsPage } from './your-rights';

@NgModule({
  declarations: [
    YourRightsPage,
  ],
  imports: [
    IonicPageModule.forChild(YourRightsPage),
  ],
})
export class YourRightsPageModule {}
