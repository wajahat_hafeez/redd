import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { EmailComposer } from '@ionic-native/email-composer';

/**
 * Generated class for the OfficesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offices',
  templateUrl: 'offices.html',
})
export class OfficesPage {

  offices:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public call:CallNumber, public sms: SMS, private emailComposer: EmailComposer)
  {
    
  }

  ionViewDidLoad() {
    this.offices= [
      {id: 1, focalperson:'Safeguard Specialist (National)',address: 'National REDD+ Office, Ministry of Climate Change Adventure Foundation Complex, Garden Avenue, Shakarparian Area, Islamabad-Pakistan', phoneNo: '00920519249187', email: ''},
      {id: 2,focalperson:'Mr. Ismail, Project Director REDD+/ Provincial REDD+ Focal Point (Gilgit)',address: 'Babar Road, Kashrote Gilgit', phoneNo: '05811920897', email: '' },
      {id:3, focalperson:'Mr. Abdul Jabbar Kazi, Conservator of Forests/ Provincial REDD+ Focal Point (Sindh)',address: 'Conservator of Forests Social Forestry Circle, Karachi, Forest Campus, Model Colony, Jinnah Avenue, Karachi', phone: '021-34510699', email: ''},
      {id: 4, focalperson:'Mr. Shahid Rasheed Awan, Additional Secretary/ Provincial REDD+ Focal Point (Punjab)',address: 'Forest, Wildlife and Fisheries Department, 38 Poonch house, Multan road, Lahore, Punjab', phoneNo: '04299210176', email: ''},
      {id: 5,focalperson:'Mr. Niaz Khan Kakar, Deputy Conservator Forest/ REDD+ Focal Point (Balochistan)',address: 'Office No. 37,38  Deputy Commissioner Office, Quetta, Balochistan', phoneNo: '0819203307', email: '' },
      {id:6, focalperson:'Mr. Hayat Ali, Divisional Forest Officer (DFO)/ REDD+ Focal Point (FATA) Bajaur Forest Division Khar',address: 'Bajaur Agency at Civil Colony Khar, Bajaur', phone: '0942221290', email: ''},
      {id: 7, focalperson:'Mr. Gohar Ali, DFO Working Plan-II/Provincial REDD+ Focal Point (KP)',address: 'DFO Working Plan Unit-II, Near Shehzada Masjid Abbotabad', phoneNo: '09929310301', email: ''},
      {id: 7, focalperson:'M.Irtaza Qureshi, DFO Working Plan Focal Point (AJK)',address: 'Jhelum Valley Division near Rashid Abad Chungi', phoneNo: '05822921397', email: ''}
      ];
  }

  callNum(i)
  {
    this.call.callNumber(this.offices[i].phoneNo, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
   }

  sendSMS(i)
  {
    let options = {
      replaceLineBreaks: true,
      android: {
      intent: 'INTENT'
      }
      };

    this.sms.send(this.offices[i].phoneNo, '', options).then(res => {
      console.log(res);
      }).catch((error) => {
      console.log(error);
      })
  }

  // sendEmail(i)
  // {
  //   let email = {
  //              to: 'max@mustermann.de'
  //    };
  //   this.emailComposer.open(email).then(res=>{
  //     console.log(res);
  //   })
  //   .catch(error=>{
  //     console.log(error);
  //   });
  
  //  }

}
