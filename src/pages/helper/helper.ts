import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HelperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-helper',
  templateUrl: 'helper.html',
})
export class HelperPage {

  loading;
  alert;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelperPage');
  }

  showLoader(view){
    this.loading = view.loadingCtrl.create({
        content: 'Please wait...'
      });
    this.loading.present();
  }

  hideLoader()
  {
      this.loading.dismiss();
  }

  showAlert(view,title,message) {
    this.alert = view.create({
      title: title,
      subTitle: message,
      buttons: ['Dismiss']
    });
    view.alert.present();
  }

}
