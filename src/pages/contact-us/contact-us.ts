import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ModelProvider } from '../../providers/model/model';

import { FormControl, FormGroup, Validators } from '@angular/forms';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';




/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
  providers: [ModelProvider]
})
export class ContactUsPage implements OnInit {

  address;
  data:any;
  addressDetail:any;
  temp:any;

  signupform: FormGroup;

  pdfObj = null;

  userData = { "fullname": "", "email": "", "address": "", "contact": "","identitycard": "","complaintcat": "" ,"desc": ""};

  loading;
  alert;




  constructor(public navCtrl: NavController, public navParams: NavParams, public geo:Geolocation, public service: ModelProvider, public alertCtrl: AlertController, public loadingCtrl:LoadingController, private plt: Platform, private file: File, private fileOpener: FileOpener)
  {
    
  }

  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signupform = new FormGroup({
      
      fullname: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(30)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      address: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(50)]),
      contact: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(11), Validators.maxLength(11)]),
      identitycard: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(13), Validators.maxLength(13)]),
      complaintcat: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(1), Validators.maxLength(200)]),
      desc: new FormControl('', [Validators.required,  Validators.minLength(5), Validators.maxLength(100)]),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }

  getlocation()
   {
     this.showLoader();
      this.geo.getCurrentPosition()
      .then((resp) =>
      {
        this.hideLoader()
        console.log(resp.coords.latitude+" , "+resp.coords.longitude);

        this.loadData(resp.coords.latitude, resp.coords.longitude)

        // this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
        // .then((result) =>
        // {
        //   console.log(result);
        //   this.address = JSON.stringify(result);
        // })
        // .catch((error: any) => console.log(error));
  
      })
      .catch((error) =>
      {
        this.showAlert("Alert",error.message);
        this.hideLoader();
        console.log('Error getting location', error.message);
      });
    }

    loadData(lat,lng){
     this.showLoader();
      var url = 'https://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+lng+'&zoom=18&addressdetails=1';
      this.service.loadGet(url)
      .then(data => {

        this.hideLoader();
        this.data = data;
        
        this.temp = (data.display_name).split(',');
        this.address = this.temp[0]+", "+this.temp[1];

      })
      .catch((error) =>
      {
        this.hideLoader();
        this.showAlert("Error", error);
        

      });
    }
    
    showLoader()
    {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
    this.loading.present();
    }

    hideLoader()
    {
      this.loading.dismiss();
    }

    showAlert(title,message) {
      this.alert = this.alertCtrl.create({
        title: title,
        subTitle: message,
        buttons: ['Dismiss']
      });
      this.alert.present();
    }





    createPdf() {
      var docDefinition = {
        content: [
          { text: 'User Information', style: 'header' },
          { text: new Date().toTimeString(), alignment: 'right' },
   
          { text: 'Username', style: 'subheader' },
          { text: this.userData.fullname },

          { text: 'Email', style: 'subheader' },
          { text: this.userData.email },

          { text: 'Contact', style: 'subheader' },
          { text: this.userData.contact },

          { text: 'ID Card No', style: 'subheader' },
          { text: this.userData.identitycard },

          { text: 'Complaint Category', style: 'subheader' },
          { text: this.userData.complaintcat },
  
          { text: 'Description', style: 'subheader' },
          { text: this.userData.desc },

          { text: 'Signature: ________________', style: 'story', margin: [0, 20, 0, 20] }
          


          // { text: this.userData.desc, style: 'story', margin: [0, 20, 0, 20] },
   
        ],
        styles: {
          header: {
            fontSize: 18,
            bold: true,
          },
          subheader: {
            fontSize: 14,
            bold: true,
            margin: [0, 15, 0, 0]
          },
          story: {
            alignment: 'right',
            width: '50%',
          }
        }
      }
      this.pdfObj = pdfMake.createPdf(docDefinition);
    }


    presentConfirmation() {
      let alert = this.alertCtrl.create({
        title: 'Confirmation',
        message: 'Do you want to download this form as PDF document?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              this.createPdf();
              this.downloadPdf();
            }
          }
        ]
      });
      alert.present();
    }

    downloadPdf() {
      if (this.plt.is('cordova')) {
        this.pdfObj.getBuffer((buffer) => {
          var blob = new Blob([buffer], { type: 'application/pdf' });
   
          // Save the PDF to the data Directory of our App
          this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
            // Open the PDf with the correct OS tools
            this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
          })
        });
      } else {
        // On a browser simply use download!
        this.pdfObj.download();
      }
    }

    submit()
    {
      var userData=
      {
        "your-name":this.userData.fullname,
        "your-subject":this.userData.address,
        "Contact":this.userData.contact,
        "your-email":this.userData.email,
        "Description":this.userData.desc,
        "IDCard":this.userData.identitycard,
        "category-complaint":this.userData.complaintcat
        
      }

       console.log(userData);
       let body = new HttpParams();

      body =  body.set('your-name', this.userData.fullname);
      body = body.set('your-email', this.userData.email);
      body = body.set('your-subject', this.userData.address);
      body = body.set('Contact', this.userData.contact);
      body = body.set('IDCard', this.userData.identitycard);
      body = body.set('category-complaint', this.userData.complaintcat);
      body = body.set('Description', this.userData.desc);

     console.log(body);

       //const emaildata = "your-name="+this.userData.fullname+"&your-email="+this.userData.email+"&your-subject="+this.userData.address+"&Contact="+this.userData.contact+"&IDCard="+this.userData.identitycard+"&category-complaint="+ this.userData.complaintcat+"&Description="+ this.userData.desc;
       this.showLoader();
      var url = 'http://webdemo.creatives.pk/redd_demo/wp-json/contact-form-7/v1/contact-forms/207259/feedback';
      this.service.loadPost(url, body)
      .then(data => {

        this.hideLoader();
        
        this.data = data;
        console.log(data);

        this.userData.fullname = '';
        this.userData.address = '';
        this.userData.complaintcat = '';
        this.userData.contact = '';
        this.userData.email = '';
        this.userData.desc = '';
        this.userData.identitycard = '';
        
        this.showAlert("Information", "Your grievance has been sent to the concerned person.");

      })
      .catch((error) =>
      {
        console.log(error);
        this.hideLoader();
        console.log("Error"+error)
        this.showAlert("Error", error.message);
      
      });

    }

}

// getlocation()
//    {
     
//       this.geo.getCurrentPosition()
//       .then((resp) =>
//       {
//         console.log(resp.coords.latitude+" , "+resp.coords.longitude);

//         this.loadData(resp.coords.latitude, resp.coords.longitude)

//         // this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
//         // .then((result) =>
//         // {
//         //   console.log(result);
//         //   this.address = JSON.stringify(result);
//         // })
//         // .catch((error: any) => console.log(error));
  
//       })
//       .catch((error) =>
//       {
//         this.showAlert("Error", error.message);
//         console.log('Error getting location', error.message)
//       });
//     }

//     loadData(lat,lng){
//      this.showLoader();
//       var url = 'https://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+lng+'&zoom=18&addressdetails=1';
//       this.service.load(url)
//       .then(data => {

//         this.hideLoader();
//         this.data = data;
        
//         this.temp = (data.display_name).split(',');
//         this.address = this.temp[0]+", "+this.temp[1];

//       })
//       .catch((error) =>
//       {
//        this.hideLoader();
//         this.showAlert("Error", error.message);
//       });
//     }

