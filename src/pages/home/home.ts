import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactUsPage } from '../contact-us/contact-us';
import { FaqPage } from '../faq/faq';
import { YourRightsPage } from '../your-rights/your-rights';
import { OfficesPage } from '../offices/offices';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  trees:any;
  

  constructor(public navCtrl: NavController) {
    this.trees= [
      {title: 'ARBORVITAE', description: 'Arborvitae (meaning "tree of life") is a medium-sized, slow-growing forest tree rather common in the northeastern part of the state, less frequent in the central and western parts.', imgsrc: 'assets/imgs/arborvitae.jpg' },
      {title: 'BLACK ASH', description: 'Black ash is a tree most commonly found in deep swamps. Occasionally, though, its found mixed with other hardwoods in moist, cold forests', imgsrc: 'assets/imgs/blackash.jpg' },
      {title: 'WHITE ASH', description: 'White ash is a valuable and rapid-growing tree in the woodlots of New York State. It is common throughout New York and is found up to an altitude of 2000 feet in the Adirondacks.', imgsrc: 'assets/imgs/whiteashtree.jpg'}
  ];
  }

  goToRights()
  {
    this.navCtrl.push(YourRightsPage);
  }

  goToGrievance()
  {
    this.navCtrl.push(ContactUsPage);
  }

  gotToFAQs()
  {
    this.navCtrl.push(FaqPage);
  }

  goToContact()
  {
    this.navCtrl.push(OfficesPage);
  }


}
