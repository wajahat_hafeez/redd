import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DescriptionPage } from '../description/description';

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

  faqs:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    

  }

  ionViewDidLoad() {
    this.faqs= [
      {title: 'What is the REDD<sup>+</sup> Safeguards Information System?',desc:'Also known as SIS. It is a free public computing platform that gathers national information on how REDD<sup>+</sup> safeguards are addressed and respected.\nThe SIS works to generate the base information for the construction of information reports for the United Nations Framework Convention on Climate Change.'},
      {title: 'Where do the Safeguards come from?',desc:'The REDD<sup>+</sup> safeguards and the need to have a Safeguard Information System for REDD+ arise from the United Nations Framework Convention on Climate Change.'},
      {title: 'Why are they important?',desc:'Because they have a rights-based approach, they seek to prevent any project or activity from having negative environmental and social effects, help to potentiate the benefits and positive impacts, ensure that projects are environmentally sustainable and help in the decision-making process in the implementation of REDD<sup>+</sup>.'},
      {title: 'What will you find in the SIS?',desc:'In the safeguards and SIS section, you can find information on how the seven REDD<sup>+</sup> safeguards are addressed and respected in the implementation of the REDD<sup>+</sup> Strategy.\nAdditionally, you will find sections with complementary information such as publications related to the REDD<sup>+</sup> process in the country.' },
      {title: 'Where can I make a complaint or suggestion?',desc:'Linked to the SIS webpage there is a Feedback and Grievance Mechanism, which details the means of access to citizens attention mechanisms, complaints and suggestions from each unit related to the implementation of the REDD<sup>+</sup> Strategy.' }
  ];
    console.log('ionViewDidLoad FaqPage');
  }

  goTo(i)
  {
    this.navCtrl.push(DescriptionPage, {
      data: this.faqs[i]
    });
  }

}
